import React from 'react'

class RoomPicker extends React.Component {
  constructor(){
    super();
    this.goToRoom = this.goToRoom.bind(this)
  }

  goToRoom(event) {
    event.preventDefault();
    //set roomId to value of form, replacing spaces with dashes for url friendly
    const roomId = this.roomInput.value.split('')
                                        .map(item => {
                                          if (item === " "){
                                            return "-"
                                          }
                                            return item
                                        }).join('');

    //transition url to https://url/room/roomId
    this.context.router.transitionTo(`room/${roomId}`);
  }

  render() {
    return(
      <form onSubmit={this.goToRoom}>
        <h2>Enter Room Name</h2>
        <input
          ref={ (input) => {this.roomInput = input} }
          type="text"
          required
          placeholder="Store Name"
        />
        <button type="submit">Create Room</button>
      </form>
    )
  }
}

RoomPicker.contextTypes = {
  router: React.PropTypes.object
}

export default RoomPicker

import React from 'react'

class RoomateForm extends React.Component {
  constructor() {
    super();
    this.createRoomate = this.createRoomate.bind(this);
  }
  createRoomate(event) {
    event.preventDefault()

    const roomate = {
      name: this.name.value
    }
    this.props.addRoomate(roomate);
    this.roomateForm.reset();
  }
  render() {
    return(
      <div>
        <h2>Add Roomates</h2>
        <form ref={ (input) => this.roomateForm = input } onSubmit={ (e) => this.createRoomate(e) }>
          <input ref={ (input) => this.name = input } type="text" placeholder="roomate name"/>
          <button type="submit">Add Roomate</button>
        </form>
      </div>
    )
  }
}

export default RoomateForm

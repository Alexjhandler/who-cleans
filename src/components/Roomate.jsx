import React from 'react'

class Roomate extends React.Component {
  render() {
    const roomate = this.props.details
    return (
      <li>{roomate.name}</li>
    )
  }
}

export default Roomate

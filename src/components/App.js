import React, { Component } from 'react';
import '../App.css';

import RoomateForm from '../components/RoomateForm';
import Roomate from '../components/Roomate'

class App extends Component {
  constructor() {
    super();
    this.addRoomate = this.addRoomate.bind(this);

    this.state = {
      roomates: {}
    }
  }
  addRoomate(roomate) {
    const roomates = {...this.state.roomates}

    const timestamp = Date.now()
    roomates[`roomate-${timestamp}`] = roomate

    this.setState({roomates})
  }
  render() {
    return (
      <div className="App">
        <RoomateForm addRoomate={this.addRoomate} />
        <ul>
          {
            Object
              .keys(this.state.roomates)
              .map(key => <Roomate key={key} index={key} details={this.state.roomates[key]}/>)
          }
        </ul>
      </div>
    );
  }
}

export default App;

import React from 'react'

const NotFound = (props) => {
  return (
    <h1>Not Found, Please check your url</h1>
  )
}

export default NotFound

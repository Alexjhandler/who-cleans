import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Match, Miss} from 'react-router';

import App from './components/App';
import NotFound from './components/NotFound'
import RoomPicker from './components/RoomPicker'
import './index.css';

const Root = () => {
  return(
    <BrowserRouter>
      <div>
        <Match exactly pattern="/" component={RoomPicker} />
        <Match pattern="/room/:roomId" component={App} />
        <Miss component={NotFound} />
      </div>
    </BrowserRouter>
  )
}

ReactDOM.render(
  <Root/>,
  document.getElementById('root')
);
